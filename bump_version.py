#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Program to set the version of poppy.

Program will update the CHANGELOG.md file that must be to set with the version value
in pyproject.toml.
Can also be used to create a setup.py file.
"""

# ________________ IMPORT _________________________
# (Include here the modules to import, e.g. import sys)
import argparse
import sys
from pathlib import Path
from datetime import datetime
from packaging.version import Version, parse, InvalidVersion

# ________________ HEADER _________________________

# Mandatory
__version__ = "1.0.0"
__author__ = "X.Bonnin"
__date__ = "13/10/2020"

# Optional
__license__ = "MIT"
__credit__ = ["RPW"]
__maintainer__ = ""
__email__ = "roc.support@sympa.obspm.fr"
__project__ = "RPW/Solar Orbiter"
__institute__ = "LESIA"
__changes__ = {"1.0.0": "first release"}

# ________________ Global Variables _____________
# (define here the global variables)

# run date
NOW = datetime.now()

# string format for datetime
TIME_OUT_STRFORMAT = "%Y%m%dT%H%M%S"

# Current dir.
CWD = Path(__file__).parent.absolute()

# CHANGELOG file (by default)
CHANGELOG_FILE = CWD / "CHANGELOG.md"

# pyproject file
PYPROJECT_FILE = CWD / "pyproject.toml"

# ________________ Class Definition __________
# (If required, define here classes)


# ________________ Global Functions __________
# (If required, define here global functions)
def valid_version(version: str) -> str:
    try:
        Version(version)
    except InvalidVersion:
        print(f"ERROR - Input Version '{version}' is no PEP440 compliant!")
        sys.exit(1)
    else:
        return version


def check_version(version: str, current_version: str, force: bool = False) -> bool:
    if version == current_version and not force:
        print(f"Current descriptor version ({version}) is already the last one")
        return False
    elif parse(version) < parse(current_version) and not force:
        print(f"Current version ({current_version}) is higher than input one {version}")
        return False
    else:
        return True


def main():
    parser = argparse.ArgumentParser(
        description="Set poppy-core version and modification message "
        "in CHANGELOG.md file"
    )
    parser.add_argument(
        "-m",
        "--modifications",
        nargs=1,
        type=str,
        default=[None],
        help="List of changes to save in CHANGELOG.md. "
        'In case of multiple changes, the items must be separated by a semi-colon ";" character',
    )
    parser.add_argument(
        "-c",
        "--changelog",
        nargs=1,
        type=Path,
        default=[CHANGELOG_FILE],
        help=f"CHANGELOG file path. Default is {CHANGELOG_FILE}.",
    )
    parser.add_argument(
        "-p",
        "--pyproject",
        nargs=1,
        type=Path,
        default=[PYPROJECT_FILE],
        help=f"pyproject.toml file path. Default is {PYPROJECT_FILE}.",
    )
    parser.add_argument(
        "--force", action="store_true", default=False, help="Force bump"
    )
    args = parser.parse_args()

    modifications = args.modifications[0]
    changelog = args.changelog[0]
    pyproject = args.pyproject[0]

    import toml

    if modifications:
        print(f"Getting version from {pyproject} ...")
        metadata = toml.load(pyproject)
        version = metadata["tool"]["poetry"]["version"]

        if not valid_version(version):
            return

        print("Done")
        print(
            f'Updating {changelog} with version "{version}" and modifications: "{modifications}" ...'
        )

        # First build the version message to insert into CHANGELOG file
        # (including break lines)
        message = [""]
        message.append(version)
        message.append("-" * len(version))
        for current_item in modifications.split(";"):
            message.append(f"- {current_item.strip()}")

        # Open current changelog file
        with open(changelog, "r") as input_file:
            metadata = input_file.readlines()

        if f"{version}\n" in metadata:
            print(f"{version} already found in {changelog}")
            if not args.force:
                return

        # Add new message at the top of the changelog file (but after title)
        title_offset = 2
        for i, current_line in enumerate(message):
            metadata.insert(title_offset + i, current_line + "\n")

        # Write into new changelog file
        with open(changelog, "w") as output_file:
            for current_line in metadata:
                output_file.write(current_line)

        print("Done")
    else:
        print(f"{changelog} not updated: no modification message passed in -m argument")


# _________________ Main ____________________________
if __name__ == "__main__":
    main()
