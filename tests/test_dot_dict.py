#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest

from poppy.core.generic.dot_dict import DotDict
from poppy.core.generic.dot_dict import SimpleDotDict


class TestDotDict(object):
    """
    Test for the behaviour of the DotDict.
    """

    @classmethod
    def setup_class(cls):
        """
        Setup the dot dict used for the tests.
        """
        cls.dotted = DotDict(
            {
                "a": 1,
                "b": 2,
                "c": DotDict(
                    {
                        "d": 3,
                        "e": 4,
                        "f": DotDict(
                            {
                                "g": DotDict({"h": 5}),
                                "i": 6,
                            }
                        ),
                    }
                ),
            }
        )

    def test_get(self):
        """
        Test that we can retrieve correctly some values.
        """
        assert self.dotted["a"] == 1
        assert self.dotted["c.d"] == 3
        assert self.dotted["c.f.g.h"] == 5

    def test_key_error(self):
        """
        Test accessing bad values.
        """
        with pytest.raises(KeyError):
            self.dotted["a.b.c"]

    def test_insertion(self):
        """
        Test that we can set a value with dot style.
        """
        a = DotDict()
        a["a"] = DotDict()
        a["a.b"] = 3
        assert a["a.b"] == 3

    def test_bad_insertion(self):
        """
        Test that we cannot set a parameter.
        """
        a = DotDict()
        with pytest.raises(KeyError):
            a["a.b"] = 3


class TestSimpleDotDict(object):
    """
    Test for the behaviour of the SimpleDotDict.
    """

    @classmethod
    def setup_class(cls):
        """
        Setup the dot dict used for the tests.
        """
        cls.dotted = SimpleDotDict(
            {
                "a": 1,
                "b": 2,
                "c": SimpleDotDict(
                    {
                        "d": 3,
                        "e": 4,
                        "f": SimpleDotDict(
                            {
                                "g": SimpleDotDict({"h": 5}),
                                "i": 6,
                            }
                        ),
                    }
                ),
            }
        )

    def test_get(self):
        """
        Test that we can retrieve correctly some values.
        """
        assert self.dotted["a"] == 1
        assert self.dotted["c.d"] == 3
        assert self.dotted["c.f.g.h"] == 5

    def test_key_error(self):
        """
        Test accessing bad values.
        """
        with pytest.raises(KeyError):
            self.dotted["a.b.c"]

    def test_insertion(self):
        """
        Test that we can set a value with dot style.
        """
        a = SimpleDotDict()
        a["a"] = SimpleDotDict()
        a["a.b"] = 3
        assert a["a.b"] == 3

    def test_bad_insertion(self):
        """
        Test that we cannot set a parameter.
        """
        a = SimpleDotDict()
        with pytest.raises(KeyError):
            a["a.b"] = 3
