#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re


def get_valid_filename(s):
    """
    Returns the given string converted to a string that can be used for a clean
    filename. Specifically, leading and trailing spaces are removed; other
    spaces are converted to underscores; and anything that is not a unicode
    alphanumeric, dash, underscore, or dot, is removed.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    text = s.strip().replace(" ", "_")
    return re.sub(r"(?u)[^-\w.]", "", text)


# vim: set tw=79 :
