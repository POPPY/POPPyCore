CHANGELOG
=========

0.12.3
------
- Update python dependencies in pyproject.toml
- Fix an error with poppy.core.test.CommandTestCase.run_command()

0.12.2
------
- Update README

0.12.1
------
- Minor update to fix the poppy logger behavior

0.12.0
------
- Apply CeCILL 2.1 license (see license in pyproject.toml and LICENSE)
- Remove pgk_resources imports (use packaging and importlib instead)
- Remove cdf and appdirs programs
- Use ruff to check and format code
- Update poetry.lock

0.11.5
------
- Update the way to get sql_type in NonNullColumn class

0.11.4
------
- Add the capability to specify database port in config file

0.11.1
------
- Update install env

0.11.0
------
- Rename package poppy.core to poppy-core

0.10.0
------
- Update python deps
- Use dash instead of dot in package name

0.9.7
-----
- Update python dependencies

0.9.6
-----
- Update dependencies (poetry.lock)
- Update build environment (.gitlab-ci.yml)

0.9.5
-----
- Hotfix in test.py

0.9.4
-----
- Manage StopIteration exception in Poppy loop (to take account of PEP 479)

0.9.3
-----
- Change POPPy license to CeCILL-C
- Minor changes

0.9.2
-----
- Minor fixes

0.9.1
-----
- Add .gitlab-ci.yml
- rename README
- update bump_version.py

0.9.0
-----
- Add pyproject.toml file to setup package

0.8.5
-----
- Add self.no_entry_point in Pipeline class

0.8.4
-----
- Minor change in Pipeline.get() method

0.8.3
-----
- Add try/except in pipeline._run_tasks() to avoid exception when loop generator is empty (no yield)

0.8.2
-----
- Add *args, **kwargs to the null_generator() method in Pipeline

0.8.1
-----
- Fix a minor bug in Pipeline.get() function that leads to inconsistent behaviour

0.8.0
-----
- Add LockFile mechanism
- Add Pipeline.exit() method

0.7.0
-----
- Add some generic exceptions

0.6.1
-----
- Force descendants/ancestors in case of error during loop tasks processing

0.6.0
-----
- Rework POPPy database to be compatible with new Target/Commands design
- Only one schema poppy is used
- Only two tables exist job_log and job_exception
- Not compatible with pipelines using POPPyCore and POP versions lesser than 0.6.0

0.5.0
-----
- Rework Target and commands design
- Fix minor bugs

0.4.0
-----
- Update maser4py (7.1 -> 8.1) and pytest (3.5 -> 4.3)
- Update pipeline templates
- Move time.py to roc.rpl plugin
- Disable abbreviations for argparse
- Fix auto search for plugins
- Update logger.json handling
- Clean useless imports
- Fix minor bugs

0.3.2
-----
- Add arguments pre-processing to quickly load the setup file and the logging level
- Move the default logger config in poppy core

0.3.1
-----
- Add jsonschema in requirements.txt
- Rename to CHANGELOG.md
- Rename to README.md
- Use CHANGELOG.md to get version for setup.py

0.3.0
-----
- Update database model

0.2.0
-----
- Major architecture update

0.1.0
-----
- First release
