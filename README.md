poppy-core
===========

[![pipeline status](https://gitlab.obspm.fr/POPPY/POPPyCore/badges/develop/pipeline.svg)](https://gitlab.obspm.fr/POPPY/POPPyCore/pipelines)

This directory contains the source code of the Plugin-Oriented Pipeline for Python (POPPy) framework core.

## Quickstart

To install the package using [pip](https://pypi.org/project/pip-tools/):

```
pip install poppy-core
```

## License

POPPY is under the license CeCILL 2.1.

## Acknowledgement

POPPY is project developed by the RPW Operations Centre (ROC) team based at LESIA (Meudon, France).
The ROC is funded by the Centre National d'Etudes Spatiale (CNES) in the framework of the European Space Agency (ESA) Solar Orbiter mission.

## Authors

xavier.bonnin@obspm.fr
florence.henry@obspm.fr

Has also contributed in the past: Sonny Lion, Gregoire Duvauchelle, Manuel Duarte
